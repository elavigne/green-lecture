'use strict';

var _ = require('lodash')
var fs = require('fs')
var path = require('path')
var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');

gulp.task('release', function(callback) {
    var path = require('path');
    var replace = require('gulp-replace');
    var config = require('./webpack.prod.config');

    gulp.src(['img/*', 'font/*'], {'base': '.'})
        .pipe(gulp.dest('build/'));

    var callbackFunc = function(err, stats) {
        if (err) {
            throw new gutil.PluginError('webpack', err);
        }

        var deleteFiles = _.difference(fs.readdirSync('build/js/'), Object.keys(stats.compilation.assets))
        _.each(deleteFiles, function(f) {
            fs.unlinkSync(path.join('./build/js/', f))
        })

        gutil.log('[webpack]', stats.toString());
        gulp.src(['pages/*.html'], {'base': 'pages'})
            .pipe(replace('common.bundle.js', stats.hash + '.common.bundle.js'))
            .pipe(replace('main.bundle.js', stats.hash + '.main.bundle.js'))
            .pipe(replace('config.bundle.js', stats.hash + '.config.bundle.js'))
            .pipe(gulp.dest('build/'))
    }

    var compiler = webpack(config);

    compiler.watch({}, callbackFunc)
});


gulp.task('dev', function(callback) {
    var WebpackDevServer = require('webpack-dev-server');
    var config = require('./webpack.dev.config');

    new WebpackDevServer(webpack(config), {
        historyApiFallback: true,
        publicPath: '/js/',
        contentBase: 'pages/'
    }).listen(8080, 'localhost', function(err) {
        if (err) {
            throw new gutil.PluginError('webpack-dev-server', err);
        }
        // Server listening
        gutil.log('[webpack-dev-server]', 'http://localhost:8080/webpack-dev-server/index.html');
    });
});
