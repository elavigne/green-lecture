'use strict'

var crypto = require('crypto')
var backbone = require('backbone')
var uuid = require('uuid')

var interpvidModels = require('interpvid/src/js/models')

var models = {}

var modifiedSync = function(method, model, options) {
    options = options || {}
    options.emulateHTTP = true
    return Backbone.sync(method, model, options);
}

var BaseModel = backbone.Model.extend({
    parse: function(res) {
        var newRes = {}

        _.each(res, function(val, key) {
            newRes[_.camelCase(key)] = val
        })

        return newRes
    },
    toJSON: function() {
        var json = backbone.Model.prototype.toJSON.apply(this, arguments)
        var res = {}

        _.each(json, function(val, key) {
            res[_.snakeCase(key)] = val
        })
        return res
    },
    sync: modifiedSync
})


models.InfoLetterModel = BaseModel.extend({
    name: 'InfoLetterModel',
    url: 'api/infoletter',
    sync: modifiedSync
})


models.ConsentFormModel = BaseModel.extend({
    name: 'ConsentFormModel',
    url: 'api/consenttext',
    sync: modifiedSync
})


models.DemographicModel = BaseModel.extend({
    name: 'DemographicModel',
    urlRoot: 'api/demographic',
    defaults: {
        id: null,
        index: 999999,
        selected: false,
        title: 'Question',
        contentText: '',
        contentType: 'text',
        choices: [],
        showPreferNotToAnswer: false,
        preferNotToAnswer: false
    },
    sync: modifiedSync
})

models.DemographicCollection = backbone.Collection.extend({
    name: 'DemographicCollection',
    url: 'api/demographic',
    model: models.DemographicModel,
    comparator: 'index',
    sync: modifiedSync
})


models.DemographicAnswersCollection = backbone.Collection.extend({
    name: 'DemographicAnswersCollection',
    url: 'api/demographic_answers',
    model: models.DemographicModel,
    sync: modifiedSync,
    _isModel: function(model) {
        return (model instanceof models.DemographicModel || backbone.Collection.prototype._isModel(model))
    }
})


models.FeedbackModel = BaseModel.extend({
    name: 'FeedbackModel',
    url: 'api/feedbacktext',
    sync: modifiedSync
})


models.ProbeModel = interpvidModels.ModalModel.extend({
    defaults: {
        id: null,
        index: 999999,
        selected: false,
        timecode: 0,
        title: 'Probe',
        contentText: '',
        contentType: 'text',
        contentAfter: '',
        startTimestamp: 0,
        submitTimestamp: 0,
        endTimestamp: 0,
        choices: [],
        answer: null,
        showPreferNotToAnswer: false,
        preferNotToAnswer: false
    },
    urlRoot: 'api/probes',
    parse: function() {
        var res = BaseModel.prototype.parse.apply(this, arguments)
        if (res.contentType) {
            res.contentType = _.camelCase(res.contentType)
        }

        return res
    },
    toJSON: function() {
        var json = BaseModel.prototype.toJSON.apply(this, arguments)
        json.timecode = parseInt(json.timecode)
        json.contentType = _.snakeCase(json.contentType)
        return json
    },
    sync: modifiedSync
})

models.ProbesCollection =  interpvidModels.ModalCollection.extend({
    name: 'ProbesCollection',
    url: 'api/probes',
    model: models.ProbeModel,
    comparator: 'index',
    sync: modifiedSync,
    hash: function() {
        var hash = crypto.createHash('sha256')
        this.each(function(m) {
            var data = m.toJSON()
            delete data.answer
            delete data.selected
            hash.update(JSON.stringify(data))
        })
        return hash.digest('hex')
    }
})

models.ProbeAnswersCollection = backbone.Collection.extend({
    name: 'ProbeAnswersCollection',
    url: 'api/probe_answers',
    model: models.ProbeModel,
    sync: modifiedSync,
    _isModel: function(model) {
        return (model instanceof models.ProbeModel || backbone.Collection.prototype._isModel(model))
    }
})


models.VideoModel = BaseModel.extend({
    urlRoot: 'api/videos',
    defaults: {
        id: null,
        index: 0,
        pauseblur: false
    },
    sync: modifiedSync
})

models.VideosCollection = backbone.Collection.extend({
    name: 'VideosCollection',
    url: 'api/videos',
    model: models.VideoModel,
    comparator: 'index',
    sync: modifiedSync
})


models.QuestionModel = BaseModel.extend({
    name: 'QuestionModel',
    urlRoot: 'api/questions',
    defaults: {
        id: null,
        index: 999999,
        timing: 'after',
        selected: false,
        title: 'New Question',
        answer: '',
        contentText: '',
        contentType: 'shortAnswer',
        contentAfter: '',
        startTimestamp: 0,
        submitTimestamp: 0,
        endTimestamp: 0,
        choices: [],
        showPreferNotToAnswer: false,
        preferNotToAnswer: false
    },
    parse: function() {
        var res = BaseModel.prototype.parse.apply(this, arguments)

        if (res.questionType) {
            res.questionType = _.camelCase(res.questionType)
        }

        if (res.index) {
            res.index = parseInt(res.index)
        }

        return res
    },
    toJSON: function() {
        var json = BaseModel.prototype.toJSON.apply(this, arguments)
        json[_.snakeCase('questionType')] = _.snakeCase(json[_.snakeCase('questionType')])
        return json
    },
    sync: modifiedSync
})

models.QuestionsCollection = backbone.Collection.extend({
    name: 'QuestionsCollection',
    url: 'api/questions',
    model: models.QuestionModel,
    comparator: 'index',
    sync: modifiedSync
})


models.QuestionsAnswersCollection = backbone.Collection.extend({
    name: 'QuestionsAnswersCollection',
    url: 'api/question_answers',
    model: models.QuestionModel,
    sync: modifiedSync,
    _isModel: function(model) {
        return (model instanceof models.QuestionModel || backbone.Collection.prototype._isModel(model))
    }
})

models.DistractionModel = BaseModel.extend({
    name: 'DistractionModel',
    defaults: {
        type: null,
        timestamp: null
    },
    idAttribute: 'timestamp'
})

models.DistractionCollection = backbone.Collection.extend({
    name: 'DistractionCollection',
    url: 'api/distractions',
    model: models.DistractionModel,
    sync: modifiedSync,
    _isModel: function(model) {
        return (model instanceof models.DistractionModel || backbone.Collection.prototype._isModel(model))
    }
})


module.exports = models