'use strict';

var $ = require('jquery')
var React = require('react')
var Router = require('react-router')
var backboneReact = require('backbone-react-component')

var Card = require('material-ui/lib/card/card')
var CardActions = require('material-ui/lib/card/card-actions')
var CardTitle = require('material-ui/lib/card/card-title')
var CardText = require('material-ui/lib/card/card-text')
var TextField = require('material-ui/lib/text-field')
var Divider = require('material-ui/lib/divider')

require('less/main.less')

var FeedbackComponent = React.createClass({
    contextTypes: {
        feedbackModel: React.PropTypes.object,
        distractionsCollection: React.PropTypes.object
    },

    componentWillMount: function() {
        backboneReact.on(this, {
            models: {
                feedbackModel: this.context.feedbackModel
            },
            collection: {
                distractionsCollection: this.context.distractionsCollection
            }
        })

        $.ajax({
            method: 'GET',
            url: 'api/get_participant_key',
            success: _.bind(function(result) {
                var key = JSON.parse(result).key
                this.setState({
                    key: key
                })
            }, this)
        })

        this.context.distractionsCollection.sync('create', this.context.distractionsCollection)
        
        
    },

    componentWillUnmount: function() {
        backboneReact.off(this)
    },

    render: function () {
        return (
            <div className="main-area">
                <Card>
                    <CardTitle title="Feedback Sheet" />
                    <CardText>
                        <div dangerouslySetInnerHTML={{__html: this.context.feedbackModel.get('text')}}></div>
                    </CardText>
                    <Divider />
                    <CardActions>
                        <TextField value={this.state.key} readonly floatingLabelText="Participation Key:" fullWidth/>
                    </CardActions>
                </Card>
            </div>
        )
    }
})

module.exports = FeedbackComponent