'use strict'

var _ = require('lodash')
var React = require('react')
var injectTapEventPlugin = require('react-tap-event-plugin')
injectTapEventPlugin()

var models = require('../models')

require('less/main.less')


var MainComponent = React.createClass({
    childContextTypes: {
        infoLetterModel: React.PropTypes.object,
        demographicCollection: React.PropTypes.object,
        consentFormModel: React.PropTypes.object,
        feedbackModel: React.PropTypes.object,
        questionsCollection: React.PropTypes.object,
        distractionsCollection: React.PropTypes.object
    },
    getChildContext: function () {
        return {
            infoLetterModel: this.infoLetterModel,
            demographicCollection: this.demographicCollection,
            consentFormModel: this.consentFormModel,
            feedbackModel: this.feedbackModel,
            questionsCollection: this.questionsCollection,
            distractionsCollection: this.distractionsCollection
        }
    },

    getInitialState: function() {
        return {
            infoLetterLoaded: false
        }
    },

    componentWillMount: function() {
        this.infoLetterModel = new models.InfoLetterModel()
        this.infoLetterModel.fetch({
            success: this._infoLetterLoaded
        })

        this.demographicCollection = new models.DemographicCollection()
        this.demographicCollection.fetch()

        this.consentFormModel = new models.ConsentFormModel()
        this.consentFormModel.fetch()

        this.feedbackModel = new models.FeedbackModel()
        this.feedbackModel.fetch()

        this.questionsCollection = new models.QuestionsCollection()
        this.questionsCollection.fetch()

        this.distractionsCollection = new models.DistractionCollection()
        
        window.addEventListener('blur', _.bind(function() {
            this.distractionsCollection.add({type: 'blur', timestamp: Date.now()})
        }, this))

        window.addEventListener('focus', _.bind(function() {
            this.distractionsCollection.add({type: 'unblur', timestamp: Date.now()})
        }, this))
    },

    _infoLetterLoaded: function() {
        this.setState({
            infoLetterLoaded: true
        })
    },

    render: function () {
        return (
            <div style={{width: '100%', height: '100%'}}>
                {this.state.infoLetterLoaded === true ? this.props.children : null}
            </div>
        )
    }
})


module.exports = MainComponent