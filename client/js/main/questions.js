'use strict'

var React = require('react')
var Router = require('react-router')
var backboneReact = require('backbone-react-component')

var interpvid = require('interpvid/src/js/index')

var models = require('../models')

require('less/main.less')

var QuestionsComponent = React.createClass({
    propTypes: {
        collection: React.PropTypes.object,
        onFinish: React.PropTypes.func
    },

    getInitialState: function() {
        return {
            index: 0
        }
    },

    _renderQuestion: function() {
        if (this.props.collection.isEmpty() === true) {
            this.props.onFinish()
        }

        var model = this.props.collection.at(this.state.index)

        if (model) {
            model.set('startTimestamp', Date.now())
            return (
                <interpvid.VideoModal key={this.state.index}
                                      model={model}
                                      onSubmitModal={this._nextQuestion}
                                      isLastOfSet={this.state.index == this.props.collection.length - 1} />
            )
        }

        return null
    },

    _nextQuestion: function() {
        var model = this.props.collection.at(this.state.index)
        model.set({
            endTimestamp: Date.now(),
            submitTimestamp: Date.now()
        })

        var nextIndex = this.state.index + 1

        if (nextIndex < this.props.collection.length) {
            this.setState({
                index: nextIndex
            })
        } else {
            this.props.onFinish()
        }
    },

    render: function () {
        return (
            <div className="main-area">
                {this._renderQuestion()}
            </div>
        )
    }
})


module.exports = QuestionsComponent