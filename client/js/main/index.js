'use strict'

var React = require('react')
var Router = require('react-router')
var ReactDOM = require('react-dom')

var MainComponent = require('./main')
var InfoLetterComponent = require('./infoletter')
var ConsentComponent = require('./consent')
var DemographicComponent = require('./demographic')
var VideosComponent = require('./videos')
var TestComponent = require('./test')
var FeedbackComponent = require('./feedback')


require('less/main.less')

var history = Router.createMemoryHistory()

ReactDOM.render((
    <Router.Router history={history}>
        <Router.Route path="/" component={MainComponent}>
            <Router.Route path="infoletter" component={InfoLetterComponent}/>
            <Router.Route path="consent" component={ConsentComponent}/>
            <Router.Route path="demographic" component={DemographicComponent}/>
            <Router.Route path="videos" component={VideosComponent}/>
            <Router.Route path="test/:timing" component={TestComponent}/>
            <Router.Route path="feedback" component={FeedbackComponent}/>
            <Router.IndexRedirect to="infoletter" />
        </Router.Route>
    </Router.Router>
), document.getElementById('view'))