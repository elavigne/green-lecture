'use strict'

var React = require('react')
var Router = require('react-router')
var backboneReact = require('backbone-react-component')

var Card = require('material-ui/lib/card/card')
var CardActions = require('material-ui/lib/card/card-actions')
var CardTitle = require('material-ui/lib/card/card-title')
var RaisedButton = require('material-ui/lib/raised-button')
var CardText = require('material-ui/lib/card/card-text')

require('less/main.less')

var InfoLetterComponent = React.createClass({
    contextTypes: {
        infoLetterModel: React.PropTypes.object,
    },

    componentWillMount: function() {
        backboneReact.on(this, {
            models: {
                infoLetterModel: this.context.infoLetterModel
            }
        })
    },

    componentWillUnmount: function() {
        backboneReact.off(this)
    },

    render: function () {
        return (
            <div className="main-area">
                <Card>
                    <CardTitle title="Information Letter" />
                    <CardText>
                        <div dangerouslySetInnerHTML={{__html: this.context.infoLetterModel.get('text')}}></div>
                    </CardText>
                    <CardActions>
                        <Router.Link to="consent">
                            <RaisedButton label="Next" primary />
                        </Router.Link>
                    </CardActions>
                </Card>
            </div>
        )
    }
})


module.exports = InfoLetterComponent