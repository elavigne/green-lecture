'use strict'

var $ = require('jquery')
var _ = require('lodash')
var backbone = require('backbone')
var React = require('react')
var ReactDOM = require('react-dom')
var Router = require('react-router')
var backboneReact = require('backbone-react-component')

var interpvid = require('interpvid/src/js/index')

var models = require('../models')

require('less/main.less')

var VideosComponent = React.createClass({
    contextTypes: {
        router: React.PropTypes.object,
        distractionsCollection: React.PropTypes.object
    },

    getInitialState: function() {
        return {
            src: null,
            videoData: null
        }
    },

    componentWillMount: function() {
        this.isBlurred = false
        this.isMuted = false
        this.answersCollection = new models.ProbeAnswersCollection()

        this.probesCollection = new models.ProbesCollection()
        this.probesCollection.fetch({success: _.bind(function() {
            this._parseData()
        }, this)})

        $.ajax({
            method: 'GET',
            url: 'api/condition',
            success: _.bind(function(result) {
                result = JSON.parse(result)
                this.video = result.video
                this.pauseblur = result.pauseblur
                this._parseData()
                this.startTimestamp = Date.now()

                this._setupDistractorMeasures()
            }, this)
        })
    },

    componentDidMount: function() {
        this.context.distractionsCollection.add({type: 'videoLoaded', timestamp: Date.now()})
    },

    componentWillUnmount: function() {
        this.context.distractionsCollection.add({type: 'videoEnded', timestamp: Date.now()})
    },

    _setupDistractorMeasures: function() {
        if (this.pauseblur === true) {
            var player = this.refs.interpvid.getVideoPlayer()

            window.addEventListener('blur', _.bind(function() {
                player.pause()
                this.isBlurred = true
            }, this))


            window.addEventListener('focus', _.bind(function() {
                if (this.isBlurred === true) {
                    player.play()
                }
                this.isBlurred = false
            }, this))
        }
    },

    _handleMute: function(evt) {
        if (this.refs.interpvid) {
            var player = this.refs.interpvid.getVideoPlayer()

            if (player.muted() || player.volume() === 0) {
                if (!this.isMuted) {
                    this.context.distractionsCollection.add({type: 'mute', timestamp: Date.now()})
                }
                this.isMuted = true
            } else {
                if (this.isMuted) {
                    this.context.distractionsCollection.add({type: 'unmute', timestamp: Date.now()})
                }
                this.isMuted = false
            }
        }
    },

    _parseData: _.after(2, function() {
        this._prepareVideo()
    }),

    _prepareVideo: function() {
        var src = 'videos/' + this.video

        var probeModels = new models.ProbesCollection(this.probesCollection.where({video: this.video}))

        this.setState({
            src: src,
            videoData: probeModels
        })
    },

    _handlePlay: _.once(function() {
        this.context.distractionsCollection.add({type: 'videoStarted', timestamp: Date.now()})
    }),

    _handleVideoEnd: function(data) {
        this.answersCollection.add(data.models)
        this.answersCollection.sync('create', this.answersCollection)

        this.endTimestamp = Date.now()
        $.ajax({
            method: 'POST',
            url: 'api/video_timestamps',
            data: JSON.stringify({
                'start_timestamp': this.startTimestamp,
                'end_timestamp': this.endTimestamp
            }),
            contentType: 'application/json',
        })


        this.context.router.push('test/after')
    },

    render: function () {
        var videoElem = null

        if (this.state.src !== null) {
            videoElem = (
                <interpvid.InterpVideo collection={this.state.videoData}
                                       onVideoEnd={this._handleVideoEnd}
                                       onMute={this._handleMute}
                                       onPlay={this._handlePlay}
                                       ref="interpvid"
                                       src={this.state.src}/>
            )
        }
        return (
            <div style={{width: '100%', height: '100%'}}>
                {videoElem}
            </div>
        )
    }
})


module.exports = VideosComponent