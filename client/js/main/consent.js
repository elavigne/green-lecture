'use strict';

var $ = require('jquery')
var React = require('react')
var Router = require('react-router')
var backboneReact = require('backbone-react-component')

var Card = require('material-ui/lib/card/card')
var CardActions = require('material-ui/lib/card/card-actions')
var CardTitle = require('material-ui/lib/card/card-title')
var RaisedButton = require('material-ui/lib/raised-button')
var CardText = require('material-ui/lib/card/card-text')
var RadioButton =  require('material-ui/lib/radio-button')
var RadioButtonGroup = require('material-ui/lib/radio-button-group')
var Divider = require('material-ui/lib/divider')

require('less/main.less')

var ConsentComponent = React.createClass({
    contextTypes: {
        consentFormModel: React.PropTypes.object
    },

    getInitialState: function() {
        return {
            nextDisabled: true
        }
    },

    componentWillMount: function() {
        backboneReact.on(this, {
            models: {
                consentFormModel: this.context.consentFormModel
            }
        })
    },

    componentWillUnmount: function() {
        backboneReact.off(this)
    },

    _handleLinkClick: function(evt) {
        if (this.state.nextDisabled) {
            evt.preventDefault()
            evt.stopPropagation()
        } else {
            $.ajax({
                type: 'POST',
                url: 'api/submit_consent',
            })
        }
    },

    _handleRadioChange: function(evt, val) {
        this.setState({
            nextDisabled: val != 'yes'
        })
    },

    render: function () {
        return (
            <div className="main-area">
                <Card>
                    <CardTitle title="Consent Form" />
                    <CardText>
                        <div dangerouslySetInnerHTML={{
                                __html: this.context.consentFormModel.get('text')
                             }}></div>
                        <br />
                        <Divider />
                        <br />
                        <div>With full knowledge of all foregoing,</div>
                        <RadioButtonGroup name="confirmation" defaultSelected="no"
                                          onChange={this._handleRadioChange}>
                            <RadioButton value="yes" label="I agree to participate"/>
                            <RadioButton value="no" label="I do not wish to participate"/>
                        </RadioButtonGroup>
                    </CardText>
                    <CardActions>
                        <Router.Link to="demographic" onClick={this._handleLinkClick}>
                            <RaisedButton label="Next" primary disabled={this.state.nextDisabled}/>
                        </Router.Link>
                    </CardActions>
                </Card>
            </div>
        )
    }
})

module.exports = ConsentComponent