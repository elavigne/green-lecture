'use strict'

var backbone = require('backbone')
var React = require('react')
var Router = require('react-router')
var backboneReact = require('backbone-react-component')

var models = require('../models')

var QuestionsComponent = require('./questions')

require('less/main.less')

var DemographicComponent = React.createClass({
    contextTypes: {
        router: React.PropTypes.object,
        demographicCollection: React.PropTypes.object,
        distractionsCollection: React.PropTypes.object
    },

    getInitialState: function() {
        return {
            initialized: false
        }
    },

    componentDidMount: function() {
        this.context.distractionsCollection.add({type: 'demographicStarted', timestamp: Date.now()})
    },

    componentWillUnmount: function() {
        this.context.distractionsCollection.add({type: 'demographicEnded', timestamp: Date.now()})
    },


    _handleFinish: function() {
        var answersCollection = new models.DemographicAnswersCollection(this.context.demographicCollection.models)
        answersCollection.sync('create', answersCollection)
        this.context.router.push('test/before')
    },

    render: function () {
        return (
            <div className="main-area">
                <QuestionsComponent collection={this.context.demographicCollection} onFinish={this._handleFinish}/>
            </div>
        )
    }
})


module.exports = DemographicComponent