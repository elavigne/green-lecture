'use strict'

var backbone = require('backbone')
var React = require('react')
var Router = require('react-router')
var backboneReact = require('backbone-react-component')

var models = require('../models')

var QuestionsComponent = require('./questions')

require('less/main.less')

var TestComponent = React.createClass({
    contextTypes: {
        router: React.PropTypes.object,
        questionsCollection: React.PropTypes.object,
        distractionsCollection: React.PropTypes.object
    },

    getInitialState: function() {
        return {
            initialized: false
        }
    },
    
    componentDidMount: function() {
        this.context.distractionsCollection.add({type: this.props.params.timing + 'TestStarted', timestamp: Date.now()})
    },

    componentWillUnmount: function() {
        this.context.distractionsCollection.add({type: this.props.params.timing + 'TestEnded', timestamp: Date.now()})
    },

    _handleFinish: function() {
        if (this.props.params.timing === 'after') {
            var answersCollection = new models.QuestionsAnswersCollection(this.context.questionsCollection.models)
            answersCollection.sync('create', answersCollection)
        }

        this.context.router.push(this.props.params.timing === 'before' ? 'videos' : 'feedback')
    },

    render: function () {
        var collection = new models.QuestionsCollection(
            this.context.questionsCollection.where({timing: this.props.params.timing})
        )

        return (
            <div className="main-area">
                <QuestionsComponent collection={collection} onFinish={this._handleFinish}/>
            </div>
        )
    }
})


module.exports = TestComponent