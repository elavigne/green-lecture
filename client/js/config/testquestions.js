'use strict'

var _ = require('lodash')
var React = require('react')
var backboneReact = require('backbone-react-component')
var uuid = require('uuid')

var FontIcon = require('material-ui/lib/font-icon')
var MenuItem = require('material-ui/lib/menus/menu-item')

var SortableList = require('./sortablelist')

var Form = require('./form')

var models = require('../models')

require('less/config.less')

var TestQuestionsComponent = React.createClass({
    contextTypes: {
        questionsCollection: React.PropTypes.object
    },

    getInitialState: function () {
        return {
            selectedQuestion: null
        }
    },

    componentWillMount: function() {
        backboneReact.on(this, {
            collections: {
                questionsCollection: this.context.questionsCollection
            }
        })
    },

    componentWillUnmount: function() {
        backboneReact.off(this)
    },


    _handleAddQuestion: function(timing, callback) {
        var questionModel = this.context.questionsCollection.create({id: uuid(), timing: timing})

        this.setState({
            selectedQuestion: questionModel.id
        }, _.partial(callback, questionModel))

        if (timing === 'before') {
            this.refs.afterList.clearSelection()
        } else if (timing === 'after') {
            this.refs.beforeList.clearSelection()
        }
    },

    _handleQuestionSelect: function(timing, questionModel, callback) {
        this.setState({
            selectedQuestion: questionModel.id
        }, _.partial(callback, questionModel))

        if (timing === 'before') {
            this.refs.afterList.clearSelection()
        } else if (timing === 'after') {
            this.refs.beforeList.clearSelection()
        }
    },

    _handleSortQuestions: function(evt) {
        this.context.questionsCollection.sync('create', this.context.questionsCollection)
    },

    render: function () {
        var model = this.context.questionsCollection.get(this.state.selectedQuestion)
        var questionComponent = null

        if (model) {
            questionComponent = (
                <Form model={model}
                      showTitle
                      key={model.id}
                      showType
                      typeLabel="Question Type"
                      showContentText
                      contentTextLabel="Question Text"
                      showPreferNotToAnswer/>
            )
        }

        var collections =  this.context.questionsCollection.partition(function(model) {
            return model.get('timing') === 'before'
        })

        var beforeCollection = new models.QuestionsCollection(collections[0])
        var afterCollection = new models.QuestionsCollection(collections[1])

        return (
            <div className="page-main">
                <div className="left-area">
                    <div style={{height: '50%', maxHeight: '50%', borderBottom: '1px solid #CCC'}}>
                        <SortableList collection={beforeCollection}
                                      ref="beforeList"
                                      title="Pre-Video Questions"
                                      onSort={this._handleSortQuestions}
                                      onAdd={_.partial(this._handleAddQuestion, 'before')}
                                      onSelect={_.partial(this._handleQuestionSelect, 'before')}/>
                    </div>
                    <div style={{height: '50%', maxHeight: '50%'}}>
                        <SortableList collection={afterCollection}
                                      ref="afterList"
                                      title="Post-Video Questions"
                                      onSort={this._handleSortQuestions}
                                      onAdd={_.partial(this._handleAddQuestion, 'after')}
                                      onSelect={_.partial(this._handleQuestionSelect, 'after')}/>
                    </div>
                </div>
                <div className="right-area questions-area">
                    {questionComponent}
                </div>
            </div>
        )
    }
})


module.exports = TestQuestionsComponent