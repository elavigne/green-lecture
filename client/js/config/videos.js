'use strict'

var _ = require('lodash')
var $ = require('jquery')
var React = require('react')
var backboneReact = require('backbone-react-component')
var uuid = require('uuid')

var interpvid = require('interpvid/src/js/index')

var Toolbar = require('material-ui/lib/toolbar/toolbar')
var ToolbarGroup = require('material-ui/lib/toolbar/toolbar-group')
var RaisedButton = require('material-ui/lib/raised-button')
var FlatButton = require('material-ui/lib/flat-button')
var Paper = require('material-ui/lib/paper')
var FontIcon = require('material-ui/lib/font-icon')
var IconButton = require('material-ui/lib/icon-button')
var Divider = require('material-ui/lib/divider')
var Checkbox = require('material-ui/lib/checkbox')
var TextField = require('material-ui/lib/text-field')
var SelectField = require('material-ui/lib/select-field')
var Menu = require('material-ui/lib/menus/menu')
var MenuItem = require('material-ui/lib/menus/menu-item')
var Dialog = require('material-ui/lib/dialog')
var LinearProgress = require('material-ui/lib/linear-progress')
var Toggle = require('material-ui/lib/toggle')

var SortableList = require('./sortablelist')
var Form = require('./form')
var models = require('../models')

require('less/config.less')

var VideoUploadDialog = React.createClass({
    getInitialState: function() {
        return {
            fileText: 'No file chosen...'
        }
    },

    _handleFileChange: function(evt) {
        this.setState({
            fileText: this.refs.fileInput.value.replace(/.*[\/\\]/, '')
        })
    },

    _handleClose: function() {
        this.props.closeHandler()
    },

    _handleSave: function() {
        this._handleFormSubmit()
        $(this.refs.fileForm).submit(this._handleFormSubmit)
    },

    _handleFormSubmit: function() {
        var formData = new FormData(this.refs.fileForm)

        this.setState({saving: true}, _.bind(function() {
            $.ajax({
                type: 'POST',
                url: 'api/uploadvideo',
                data: formData,
                contentType: false,
                processData: false
            }).success(this._handleFinishSave)
                .error(this._handleFinishError)
        }, this))

        return false
    },

    _handleUploadClick: function() {
        this.refs.fileInput.click()
    },

    _handleFinishSave: function() {
        this.setState({saving: false})
        this.props.saveHandler()
    },

    _handleFinishError: function(error) {
        this.setState({saving: false})
        alert('Could not upload video: ' + error.toString())
    },

    render: function () {
        var dialogActions = [
            <FlatButton label="Save" onClick={this._handleSave}/>,
            <FlatButton label="Cancel" onClick={this._handleClose}/>
        ]

        return (
            <Dialog title="Upload Video"
                    actions={dialogActions}
                    modal={true}
                    open={this.props.dialogOpen}
                    onRequestClose={this._closeDialog}>
                {
                    this.state.saving ? <LinearProgress mode="indeterminate" style={{height:20}}/> : (
                        <div style={{display: 'flex'}}>
                            <RaisedButton label="Choose a File" secondary onClick={this._handleUploadClick}>
                                <form ref="fileForm" method="post" encType="multipart/form-data">
                                    <input ref="fileInput" type="file" name="videofile"
                                           onChange={this._handleFileChange}
                                           style={{display: 'none'}}/>
                                </form>
                            </RaisedButton>
                            <Paper zDepth={1} style={{flexGrow: 1, verticalAlign: 'middle'}}>
                                <span style={{lineHeight: '36px', paddingLeft: 10}}>{this.state.fileText}</span>
                            </Paper>
                        </div>
                    )
                }

            </Dialog>
        )
    }
})

var VideosComponent = React.createClass({
    contextTypes: {
        videosCollection: React.PropTypes.object,
        probesCollection: React.PropTypes.object
    },

    componentWillMount: function() {
        backboneReact.on(this, {
            collections: {
                videosCollection: this.context.videosCollection,
                probesCollection: this.context.probesCollection
            }
        })
    },

    componentWillUnmount: function() {
        backboneReact.off(this)
    },

    getInitialState: function () {
        return {
            dialogOpen: false,
            selectedVideo: null
        }
    },

    _handleVideoClick: function(model, callback) {
        this.setState({
            selectedVideo: model.get('id')
        }, _.partial(callback, model))
    },

    _handleAddVideo: function () {
        this.setState({
            dialogOpen: true
        })
    },

    _handleDeleteVideo: function (model, callback) {
        if (confirm('Are you sure you wish to permanently delete ' + model.get('id') + '?')) {
            this.setState({
                selectedVideo: null
            }, _.bind(function(){
                callback(model)
            }, this))
        }
    },

    _saveVideo: function () {
        this._closeDialog()
        this.context.videosCollection.fetch()
    },

    _handleAddProbe: function(callback) {
        var probeModel = this.context.probesCollection.create({video: this.state.selectedVideo, id: uuid()})

        this.setState({
            selectedProbe: probeModel.id
        }, _.partial(callback, probeModel))
    },

    _handleDeleteProbe: function(model, evt) {
        evt.stopPropagation()
        model.destroy()
    },

    _handleProbeClick: function(model, callback) {
        this.setState({
            selectedProbe: model.id
        }, _.partial(callback, model))
    },

    _closeDialog: function () {
        this.setState({
            dialogOpen: false
        })
    },

    _createProbeSection: function() {
        var model = this.context.probesCollection.get(this.state.selectedProbe)
        if (model) {
            return (
                <Form model={model}
                      showTitle
                      key={model.id}
                      showType
                      typeLabel="Probe Type"
                      showTimecode
                      showContentText
                      contentTextLabel="Probe Text"
                      showContentAfter
                      showPreferNotToAnswer/>
            )
        }

        return null
    },

    _refreshVideo: function() {
        this.forceUpdate()
    },

    _handleSort: function() {
        this.context.probesCollection.sync('create', this.context.probesCollection)
    },

    _handlePauseBlurToggle: function(videoModel) {
        videoModel.set('pauseblur', !videoModel.get('pauseblur'))
        this.context.videosCollection.sync('create', this.context.videosCollection)
    },

    _createRightSection: function() {
        if (this.state.selectedVideo !== null) {
            this.filteredProbesCollection = new models.ProbesCollection(
                this.context.probesCollection.filter(_.bind(function(model) {
                    return model.get('video') == this.state.selectedVideo
                }, this)))
            
            let videoModel = this.context.videosCollection.get(this.state.selectedVideo)

            return (
                <div className="right-area">
                    <div style={{width: '330px', height: 'calc(100% - 200px)', position: 'absolute', top: 0, left: 0}}>
                        <SortableList collection={this.filteredProbesCollection}
                                      title="Probes"
                                      addIcon="add"
                                      onSort={this._handleSort}
                                      onAdd={this._handleAddProbe}
                                      onSelect={this._handleProbeClick} />
                    </div>
                    <div style={{width: '330px', height: '200px', position: 'absolute', bottom: 0, left: -1, borderTop: '1px solid #ccc', borderRight: '1px solid #ccc'}}>
                        <MenuItem>Other</MenuItem>
                        <div style={{padding: 10}}>
                            <Toggle defaultToggled={videoModel.get('pauseblur')}
                                    onToggle={_.partial(this._handlePauseBlurToggle, videoModel)}
                                    label={'Pause video on "blur"'}
                                    labelPosition="right"/>
                        </div>
                    </div>
                    <div style={{width: 'calc(100% - 330px)', height: '100%', position: 'absolute', top: 0, right: 0}}>
                        <div style={{height: '50%'}}>
                            <interpvid.InterpVideo
                                key={this.filteredProbesCollection.hash()}
                                videoOptions={{resizeOptions: {shortWindowVideoHeightAdjustment: 64}}}
                                collection={this.filteredProbesCollection}
                                src={"videos/" + this.state.selectedVideo}
                                onVideoEnd={this._refreshVideo}
                                enableSeeking
                                enablePausing/>
                        </div>
                        <div style={{height: '50%', paddingLeft: 10}}>
                            {this._createProbeSection()}
                        </div>
                    </div>
                </div>
            )
        }

        return null
    },

    render: function () {
        return (
            <div className="page-main">
                <VideoUploadDialog dialogOpen={this.state.dialogOpen}
                                   saveHandler={this._saveVideo}
                                   closeHandler={this._closeDialog} />
                <div className="left-area">
                    <div style={{height: '100%', maxHeight: '100%'}}>
                        <SortableList collection={this.context.videosCollection}
                                      title="Videos"
                                      addIcon="file_upload"
                                      textKey="id"
                                      onAdd={this._handleAddVideo}
                                      onDelete={this._handleDeleteVideo}
                                      onSelect={this._handleVideoClick} />
                    </div>
                </div>
                {this._createRightSection()}
            </div>
        )
    }
})


module.exports = VideosComponent
