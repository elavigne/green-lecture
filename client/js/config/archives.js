'use strict'

var _ = require('lodash')
var $ = require('jquery')
var React = require('react')

var GridList = require('material-ui/lib/grid-list/grid-list')
var GridTile = require ('material-ui/lib/grid-list/grid-tile')

var IconMenu = require('material-ui/lib/menus/icon-menu')
var MenuItem = require('material-ui/lib/menus/menu-item')
var IconButton = require('material-ui/lib/icon-button')
var MoreVertIcon = require('material-ui/lib/svg-icons/navigation/more-vert')
var FolderIcon = require('material-ui/lib/svg-icons/file/folder')
var FileIcon = require('material-ui/lib/svg-icons/editor/insert-drive-file')
var Toolbar = require('material-ui/lib/toolbar/toolbar')
var ToolbarSeparator = require('material-ui/lib/toolbar/toolbar-separator')
var FlatButton = require('material-ui/lib/flat-button')

var TextComponent = require('./text')

require('less/config.less')

var ArchivesComponent = React.createClass({
    contextTypes: {
        fileCollection: React.PropTypes.object
    },

    getInitialState: function() {
        return {
            currentPath: []
        }
    },

    componentWillMount : function() {
        this._refreshFiles()
    },

    componentDidUpdate: function() {
        if (this.refs.input) {
            _.delay(_.bind(function() {
                this.refs.input.focus()
                this.refs.input.select()
            }, this), 400)
        }
    },

    _refreshFiles: function() {
        $.ajax({
            method: 'GET',
            url: 'api/archives',
            success: _.bind(function(result) {
                this.setState({
                    files: JSON.parse(result).files
                })
            }, this)
        })
    },

    _handleFolderEnter: function(key) {
          this.setState({
              currentPath: _.concat(this.state.currentPath, key)
          })
    },

    _handleBreadcrumbClick: function(index) {
        this.setState({
              currentPath: _.slice(this.state.currentPath, 0, index + 1)
          })
    },

    _handleDownload: function(key) {
        this.refs.iframe.src = 'api/download_files?path=' + JSON.stringify(_.concat([], this.state.currentPath, key))
    },

    _handleDelete: function(key) {
        if (confirm('Are you sure you wish to delete "/' +
                _.join(_.concat(this.state.currentPath, key), '/') +
                '" ? It will be lost forever.\n\n' +
                'It is recommended to at least download or archive this directory/file before deleting.') === true) {
            $.ajax({
                method: 'GET',
                url: 'api/delete_files?path=' + JSON.stringify(_.concat([], this.state.currentPath, key)),
                success: _.bind(function(result) {
                    this._refreshFiles()
                }, this)
            })
        }
    },

    _handleArchive: function(key) {
        $.ajax({
            method: 'GET',
            url: 'api/archive_files',
            success: _.bind(function (result) {
                var archiveFolder = JSON.parse(result).archive
                this._refreshFiles()
                this.setState({
                    currentPath: ['archives'],
                    editingFile: archiveFolder
                })
            }, this)
        })
    },

    _handleRename: function(oldName, newName) {
        $.ajax({
                method: 'GET',
                url: 'api/rename_file?oldname=' + oldName + '&newname=' + newName,
                success: _.bind(function() {
                    this._refreshFiles()
                    this.setState({
                        editingFile: null
                    })
                }, this)
            })
    },

    _handleInputChange: function(key, evt) {
        var value = evt.target.value
        var currentFiles = _.keys(_.get(this.state.files, this.state.currentPath, this.state.files))
        if (_.includes(currentFiles, value)) {
            evt.target.style.backgroundColor = 'red'
            evt.target.style.color = 'white'
        } else {
            evt.target.style.backgroundColor = 'white'
            evt.target.style.color = 'black'

            if (evt.keyCode === 13) {
                this._handleRename(key, value)
            } else if (evt.keyCode === 27) {
                this.setState({
                    editingFile: null
                })
            }
        }
    },

    _showRename: function(key) {
        this.setState({
            editingFile: key
        })
    },

    _createRenameInput: function(key) {
        if (this.state.editingFile === key) {
            return (
                <input ref="input" placeholder={key} type="text" onKeyUp={_.partial(this._handleInputChange, key)}/>
            )
        }
        return key
    },

    render: function () {
        var iconStyle = {
            width: 72,
            height: 72,
            display: 'block',
            marginLeft: 'auto',
            marginRight: 'auto'
        }

        var currentFiles = _.get(this.state.files, this.state.currentPath, this.state.files)

        return (
            <div className="page-main">
                <iframe ref="iframe" style={{display: 'none'}} ></iframe>
                <Toolbar>
                    <ToolbarSeparator style={{visibility: 'hidden', margin: 0}}/>
                    <FlatButton label="Home" onTouchTap={_.partial(this._handleBreadcrumbClick, -1)}/>
                    {_.flatten(_.map(this.state.currentPath, _.bind(function(path, index) {
                        return [
                            <ToolbarSeparator style={{marginLeft: 12, marginRight: 12}}/>,
                            <FlatButton label={path} onTouchTap={_.partial(this._handleBreadcrumbClick, index)}/>
                        ]
                    }, this)))}
                </Toolbar>
                <div>
                    {
                        _.map(_.keys(currentFiles).sort(function(a, b) { return a.localeCompare(b) }),
                            _.bind(function(key) {
                                return (
                                    <GridTile
                                        key={key}
                                        title={this._createRenameInput(key)}
                                        style={{width: 200, height: 130, float: 'left', margin: 5}}
                                        actionIcon={
                                            <IconMenu
                                              iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                                              anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                                              targetOrigin={{horizontal: 'left', vertical: 'top'}}>
                                              {
                                                    this.state.currentPath.length == 0
                                                        && key === 'current'
                                                        && <MenuItem primaryText="Archive"
                                                                     onTouchTap={_.partial(this._handleArchive, key)}/>
                                              }
                                              {
                                                    (this.state.currentPath.length !== 0
                                                        || key == 'current')
                                                        && <MenuItem primaryText="Delete"
                                                                     onTouchTap={_.partial(this._handleDelete, key)}/>
                                              }
                                              {
                                                    this.state.currentPath.length === 1
                                                        && this.state.currentPath[0] === 'archives'
                                                        && <MenuItem primaryText="Rename"
                                                                     onTouchTap={_.partial(this._showRename, key)}/>
                                              }
                                              <MenuItem primaryText="Download"
                                                        onTouchTap={_.partial(this._handleDownload, key)}/>
                                            </IconMenu>
                                        }
                                    >
                                        {_.isObject(currentFiles[key])
                                            ? <FolderIcon style={_.merge({cursor: 'pointer'}, iconStyle)}
                                                          title={key}
                                                          onTouchTap={_.partial(this._handleFolderEnter, key)}/>
                                            : <FileIcon style={iconStyle} title={key}/>
                                        }
                                    </GridTile>
                                )
                            }, this)
                        )
                    }
                </div>
            </div>
        )
    }
})


module.exports = ArchivesComponent