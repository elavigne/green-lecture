'use strict'

var React = require('react')

var TextComponent = require('./text')

require('less/config.less')

var InfoLetterComponent = React.createClass({
    contextTypes: {
        infoLetterModel: React.PropTypes.object
    },

    render: function () {
        return (
            <div className="page-main">
                <TextComponent textModel={this.context.infoLetterModel} />
            </div>
        )
    }
})


module.exports = InfoLetterComponent