'use strict'

var React = require('react')

var TextComponent = require('./text')

require('less/config.less')

var ConsentComponent = React.createClass({
    contextTypes: {
        consentFormModel: React.PropTypes.object
    },

    render: function () {
        return (
            <div className="page-main">
                <TextComponent textModel={this.context.consentFormModel} />
            </div>
        )
    }
})


module.exports = ConsentComponent