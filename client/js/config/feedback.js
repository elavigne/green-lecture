'use strict'

var React = require('react')

var TextComponent = require('./text')

require('less/config.less')

var FeedbackComponent = React.createClass({
    contextTypes: {
        feedbackModel: React.PropTypes.object
    },

    render: function () {
        return (
            <div className="page-main">
                <TextComponent textModel={this.context.feedbackModel} />
            </div>
        )
    }
})


module.exports = FeedbackComponent