'use strict'

var _ = require('lodash')
var React = require('react')
var backboneReact = require('backbone-react-component')
var uuid = require('uuid')

var SortableList = require('./sortablelist')

var Form = require('./form')

require('less/config.less')

var DemographicComponent = React.createClass({
    contextTypes: {
        demographicCollection: React.PropTypes.object
    },

    getInitialState: function () {
        return {
            selectedQuestion: null
        }
    },

    componentWillMount: function() {
        backboneReact.on(this, {
            collections: {
                demographicCollection: this.context.demographicCollection
            }
        })
    },

    componentWillUnmount: function() {
        backboneReact.off(this)
    },


    _handleAddQuestion: function(callback) {
        var demographicModel = this.context.demographicCollection.create({id: uuid()})

        this.setState({
            selectedQuestion: demographicModel.id
        }, _.partial(callback, demographicModel))
    },

    _handleQuestionSelect: function(demographicModel, callback) {
        this.setState({
            selectedQuestion: demographicModel.id
        }, _.partial(callback, demographicModel))
    },

    render: function () {
        var model = this.context.demographicCollection.get(this.state.selectedQuestion)
        var questionComponent = null

        if (model) {
            questionComponent = (
                <Form model={model}
                      showTitle
                      key={model.id}
                      showType
                      typeLabel="Question Type"
                      showContentText
                      contentTextLabel="Question Text"
                      showPreferNotToAnswer/>
            )
        }

        return (
            <div className="page-main">
                <div className="left-area">
                    <div style={{height: '100%', maxHeight: '100%'}}>
                        <SortableList collection={this.context.demographicCollection}
                                      title="Demographic Questions"
                                      textKey="title"
                                      onAdd={this._handleAddQuestion}
                                      onSelect={this._handleQuestionSelect}/>
                    </div>
                </div>
                <div className="right-area questions-area">
                    {questionComponent}
                </div>
            </div>
        )
    }
})


module.exports = DemographicComponent