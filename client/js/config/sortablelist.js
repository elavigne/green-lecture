'use strict'

var _ = require('lodash')
var React = require('react')
var backboneReact = require('backbone-react-component')
var SortableMixin = require('sortablejs/react-sortable-mixin')
var uuid = require('uuid')

var FontIcon = require('material-ui/lib/font-icon')
var Divider = require('material-ui/lib/divider')
var Menu = require('material-ui/lib/menus/menu')
var MenuItem = require('material-ui/lib/menus/menu-item')
var List = require('material-ui/lib/lists/list')
var ListItem = require('material-ui/lib/lists/list-item')

require('less/config.less')

var SortableItem = React.createClass({
    render: function() {
        var leftIcon = (
            <i className="material-icons drag-handle">
                drag_handle
            </i>
        )

        var rightIcon = (
            <FontIcon className="material-icons" style={{color: '#ff8080'}}
                      onClick={_.partial(this.props.onDelete, this.props.model)}>
                cancel
            </FontIcon>
        )

        var style = {minHeight: 48}

        if (this.props.selected) {
            style.backgroundColor = '#666'
            style.color = 'white'
        }

        return (
            <div className="sortable-item" key={this.props.model.id} >
                <ListItem onClick={_.partial(this.props.onSelect, this.props.model)}
                          disableTouchRipple
                          value={this.props.model.id}
                          style={style}
                          innerDivStyle={{maxWidth:'100%', paddingRight:'52px',
                                          whiteSpace:'nowrap', overflow: 'hidden',
                                          textOverflow: 'ellipsis'}}
                          rightIcon={rightIcon}
                          leftIcon={leftIcon}>
                    <div style={{whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis'}}
                         title={this.props.text}>
                        {this.props.text}
                    </div>
                </ListItem>
            </div>
        )
    }
})

var SortableContainer = React.createClass({
    mixins: [SortableMixin],

    sortableOptions: {
        handle: '.drag-handle',
        draggable: '.sortable-item'
    },

    getInitialState: function() {
        return {
            items: this.props.collection.pluck('id')
        }
    },

    componentWillReceiveProps: function(props) {
        this.setState({
            items: props.collection.pluck('id')
        })
    },

    handleSort: function(evt) {
        _.each(this.state.items, _.bind(function(id, index) {
            var model = this.props.collection.get(id)
            model.set('index', index)
        }, this))

        this.props.collection.sort()

        if (_.isFunction(this.props.onSort)) {
            this.props.onSort(evt)
        } else {
            this.props.collection.sync('create', this.props.collection)
        }
    },

    render: function() {
        var items = _.map(this.state.items, _.bind(function(id) {
            var model = this.props.collection.get(id)
            var props = {
                key: model.id,
                model: model,
                text: model.get(this.props.textKey),
                onDelete: this.props.onDelete,
                onSelect: this.props.onSelect
            }

            if (this.props.selectedItem == model.id) {
                props.selected = true
            }

            return (
                <SortableItem {...props}/>
            )
        }, this))

        return (
            <div>{items}</div>
        )
    }
})

var SortableList = React.createClass({

    propTypes: {
        collection: React.PropTypes.object,
        addIcon: React.PropTypes.string,
        title: React.PropTypes.node,
        textKey: React.PropTypes.string
    },

    getDefaultProps: function() {
        return {
            title: 'Questions',
            addIcon: 'add',
            textKey: 'title'
        }
    },

    getInitialState: function() {
        return {
            selectedItem: null
        }
    },

    _handleItemAdd: function(evt) {
        var callback = _.bind(function(model) {
            this.setState({
                selectedItem: model.id
            })
        }, this)

        if (_.isFunction(this.props.onAdd)) {
            this.props.onAdd(callback)
        } else {
            var model = this.props.collection.create({id: uuid()})
            callback(model)
        }
    },

    _handleItemSelect: function(model, evt) {
        var callback = _.bind(function(model) {
            this.setState({
                selectedItem: model.id
            })
        }, this)

        if (_.isFunction(this.props.onSelect)) {
            this.props.onSelect(model, callback)
        } else {
            callback(model)
        }
    },

    _handleItemDelete: function(model, evt) {
        evt.stopPropagation()

        var callback = _.bind(function(model) {
            model.destroy()
        }, this)

        if (_.isFunction(this.props.onDelete)) {
            this.props.onDelete(model, callback)
        } else {
            callback(model)
        }
    },

    clearSelection: function() {
        this.setState({
            selectedItem: null
        })
    },

    render: function() {
        return (
            <div style={{height: '100%', maxHeight: '100%', overflowY: 'auto'}}>
                <List autoWidth={false} width="330px"
                      style={{borderRight: '1px solid #CCC', height: '100%'}}
                      value={this.state.selectedItem}>
                    <ListItem style={{color: 'black'}} primaryText={this.props.title}
                              disabled={true}
                              rightIcon={
                              <FontIcon className="material-icons"
                                        style={{color: '#006400', cursor: 'pointer'}}
                                        onClick={this._handleItemAdd}>
                                  {this.props.addIcon}
                              </FontIcon>
                          }/>
                    <Divider />
                    <SortableContainer collection={this.props.collection}
                                       selectedItem={this.state.selectedItem}
                                       textKey={this.props.textKey}
                                       onSort={this.props.onSort}
                                       onSelect={this._handleItemSelect}
                                       onDelete={this._handleItemDelete}/>
                </List>
            </div>
        )

    }
})



module.exports = SortableList