'use strict'

var $ = require('jquery')
var React = require('react')
var Router = require('react-router')
var AppBar = require('material-ui/lib/app-bar')
var LeftNav = require('material-ui/lib/left-nav')
var IconMenu = require('material-ui/lib/menus/icon-menu')
var IconButton = require('material-ui/lib/icon-button')
var MenuItem = require('material-ui/lib/menus/menu-item')
var Divider = require('material-ui/lib/divider')
var FlatButton = require('material-ui/lib/flat-button')
var injectTapEventPlugin = require('react-tap-event-plugin')
var Dialog = require('material-ui/lib/dialog')
var RaisedButton = require('material-ui/lib/raised-button')
var Paper = require('material-ui/lib/paper')
var LinearProgress = require('material-ui/lib/linear-progress')
injectTapEventPlugin()

var models = require('../models')

require('less/main.less')
require('less/config.less')

var SideBarItem = React.createClass({
    render: function () {
        return (
            <Router.Link className="sidebar-item" activeClassName="sidebar-item-active"
                         to={this.props.link}>
                <MenuItem>{this.props.text}</MenuItem>
            </Router.Link>
        )
    }
})

var SideBar = React.createClass({
    render: function () {
        return (
            <LeftNav style={this.props.style}>
                <SideBarItem link="infoletter" text="Information Letter"/>
                <SideBarItem link="consent" text="Consent Form"/>
                <SideBarItem link="demographic" text="Demographic"/>
                <SideBarItem link="feedback" text="Feedback Sheet"/>
                <Divider />
                <SideBarItem link="videos" text="Videos"/>
                <SideBarItem link="testquestions" text="Test Questions"/>
                <Divider />
                <SideBarItem link="archives" text="Archives" />
            </LeftNav>
        )
    }
})



var ImportConfigDialog = React.createClass({
    getInitialState: function() {
        return {
            fileText: 'No file chosen...',
            saving: false
        }
    },

    _handleFileChange: function(evt) {
        this.setState({
            fileText: this.refs.fileInput.value.replace(/.*[\/\\]/, '')
        })
    },

    _handleClose: function() {
        this.props.closeHandler()
    },

    _handleSave: function() {
        if (!confirm('Are you sure you wish to replace your entire configuration with this new one?')) {
            return
        }
        this._handleFormSubmit()
        $(this.refs.fileForm).submit(this._handleFormSubmit)
    },

    _handleFormSubmit: function() {
        var formData = new FormData(this.refs.fileForm)

        this.setState({saving: true}, _.bind(function() {
            $.ajax({
                type: 'POST',
                url: 'api/importconfig',
                data: formData,
                contentType: false,
                processData: false
            }).success(this._handleFinishSave)
                .error(this._handleFinishError)
        }, this))

        return false
    },

    _handleFinishSave: function() {
        this.setState({saving: false})
        this.props.saveHandler()
    },

    _handleFinishError: function(error) {
        this.setState({saving: false})
        alert('Could not import configuration: ' + error.toString())
    },

    _handleUploadClick: function() {
        this.refs.fileInput.click()
    },

    render: function () {
        var dialogActions = this.state.saving
            ? []
            : [
            <FlatButton label="Save" onClick={this._handleSave}/>,
            <FlatButton label="Cancel" onClick={this._handleClose}/>
        ]


        return (
            <Dialog title={this.state.saving ? 'Importing Config' : 'Import Config'}
                    actions={dialogActions}
                    modal={true}
                    open={this.props.dialogOpen}
                    onRequestClose={this._closeDialog}>
                {
                    this.state.saving ? <LinearProgress mode="indeterminate" style={{height:20}}/> : (
                        <div style={{display: 'flex'}}>
                            <RaisedButton label="Choose a File" secondary onClick={this._handleUploadClick}>
                                <form ref="fileForm" method="post" encType="multipart/form-data">
                                    <input ref="fileInput" type="file" name="configfile"
                                           onChange={this._handleFileChange}
                                           style={{display: 'none'}}/>
                                </form>
                            </RaisedButton>
                            <Paper zDepth={1} style={{flexGrow: 1, verticalAlign: 'middle'}}>
                                <span style={{lineHeight: '36px', paddingLeft: 10}}>{this.state.fileText}</span>
                            </Paper>
                        </div>
                    )
                }
            </Dialog>
        )
    }
})

var ConfigComponent = React.createClass({
    childContextTypes: {
        infoLetterModel: React.PropTypes.object,
        consentFormModel: React.PropTypes.object,
        demographicCollection: React.PropTypes.object,
        feedbackModel: React.PropTypes.object,
        probesCollection: React.PropTypes.object,
        videosCollection: React.PropTypes.object,
        questionsCollection: React.PropTypes.object
    },
    getChildContext: function () {
        return {
            infoLetterModel: this.infoLetterModel,
            consentFormModel: this.consentFormModel,
            demographicCollection: this.demographicCollection,
            feedbackModel: this.feedbackModel,
            probesCollection: this.probesCollection,
            videosCollection: this.videosCollection,
            questionsCollection: this.questionsCollection
        }
    },

    getInitialState: function() {
        return {
            dialogOpen: false
        }
    },

    componentWillMount: function() {
        this.infoLetterModel = new models.InfoLetterModel()
        this.infoLetterModel.fetch()

        this.consentFormModel = new models.ConsentFormModel()
        this.consentFormModel.fetch()

        this.demographicCollection = new models.DemographicCollection()
        this.demographicCollection.fetch()

        this.feedbackModel = new models.FeedbackModel()
        this.feedbackModel.fetch()

        this.probesCollection = new models.ProbesCollection()
        this.probesCollection.fetch()

        this.videosCollection = new models.VideosCollection()
        this.videosCollection.fetch()

        this.questionsCollection = new models.QuestionsCollection()
        this.questionsCollection.fetch()
    },

    _downloadData: function() {
        window.location = 'api/downloaddata'
    },

    _exportConfig: function() {
        window.location = 'api/exportconfig'
    },

    _openDialog: function () {
        this.setState({
            dialogOpen: true
        })
    },

    _closeDialog: function () {
        this.setState({
            dialogOpen: false
        })
    },

    render: function () {
        var rightIconMenu = (
            <IconMenu targetOrigin={{horizontal: 'right', vertical: 'top'}}
                      anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                      iconButtonElement={
                        <IconButton iconClassName="material-icons">more_vert</IconButton>
                      }>
                <MenuItem primaryText="Download Data" onClick={this._downloadData} />
                <MenuItem primaryText="Import Config" onClick={this._openDialog}/>
                <MenuItem primaryText="Export Config" onClick={this._exportConfig} />
            </IconMenu>
        )
        return (
            <div style={{width: '100%', height: 'calc(100% - 64px)'}}>
                <AppBar title="Green Lecture Configuration"
                        showMenuIconButton={false}
                        iconElementRight={rightIconMenu} />
                <SideBar style={{top: 64, height: 'calc(100% - 64px)', zIndex: 1}}/>
                <div style={{paddingLeft: 256, width: '100%', height: '100%',
                             boxSizing: 'border-box', position: 'relative'}}>
                    {this.props.children}
                </div>
                <ImportConfigDialog dialogOpen={this.state.dialogOpen}
                                    saveHandler={this._closeDialog}
                                    closeHandler={this._closeDialog}/>
            </div>
        )
    }
})


module.exports = ConfigComponent