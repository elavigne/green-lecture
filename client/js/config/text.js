'use strict'

var React = require('react')
var Editor = require('react-quill')
var backboneReact = require('backbone-react-component')

var quillConfig = require('./quillconfig')

require('less/config.less')

var TextComponent = React.createClass({
    componentWillMount: function() {
        backboneReact.on(this, {
            models: {
                model: this.props.textModel
            }
        })
    },

    componentWillUnmount: function() {
        backboneReact.off(this)
    },

    _handleOnChange: function(value, delta, source) {
        this.props.textModel.set('text', value)
        this.props.textModel.save()
    },

    render: function () {
        return (
            <Editor className="config-editor" theme="snow" value={this.state.model.text}
                    onChange={this._handleOnChange}
                    toolbar={quillConfig.toolbarItems}/>
        )
    }
})


module.exports = TextComponent