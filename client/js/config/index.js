'use strict'

var React = require('react')
var Router = require('react-router')
var ReactDOM = require('react-dom')

var ConfigComponent = require('./main')
var InfoLetterComponent = require('./infoletter')
var ConsentComponent = require('./consent')
var DemographicComponent = require('./demographic')
var FeedbackComponent = require('./feedback')
var VideosComponent = require('./videos')
var TestQuestionsComponent = require('./testquestions')
var ArchivesComponent = require('./archives')

require('less/main.less')

ReactDOM.render((
    <Router.Router>
        <Router.Route path="/" component={ConfigComponent}>
            <Router.Route path="infoletter" component={InfoLetterComponent}/>
            <Router.Route path="consent" component={ConsentComponent}/>
            <Router.Route path="demographic" component={DemographicComponent}/>
            <Router.Route path="feedback" component={FeedbackComponent}/>
            <Router.Route path="videos" component={VideosComponent}/>
            <Router.Route path="testquestions" component={TestQuestionsComponent}/>
            <Router.Route path="archives" component={ArchivesComponent}/>
            <Router.IndexRedirect to="infoletter" />
        </Router.Route>
    </Router.Router>
), document.getElementById('view'))