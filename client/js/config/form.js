'use strict'

var _ = require('lodash')
var React = require('react')
var backboneReact = require('backbone-react-component')
var uuid = require('uuid')

var TextField = require('material-ui/lib/text-field')
var SelectField = require('material-ui/lib/select-field')
var MenuItem = require('material-ui/lib/menus/menu-item')
var Toggle = require('material-ui/lib/toggle')

var models = require('../models')

require('less/config.less')

var QuestionForm = React.createClass({
    propTypes: {
        showTitle: React.PropTypes.bool,
        titleLabel: React.PropTypes.string,
        showType: React.PropTypes.bool,
        typeLabel: React.PropTypes.string,
        showTimecode: React.PropTypes.bool,
        timecodeLabel: React.PropTypes.string,
        showContentText: React.PropTypes.bool,
        contentTextLabel: React.PropTypes.string,
        showContentAfter: React.PropTypes.bool,
        contentAfterLabel: React.PropTypes.string,
        showAnswerText: React.PropTypes.bool,
        answerTextLabel: React.PropTypes.string,
        showPreferNotToAnswer: React.PropTypes.bool
    },

    getDefaultProps: function() {
        return {
            titleLabel: 'Title',
            typeLabel: 'Type',
            timecodeLabel: 'Timecode (in seconds)',
            contentTextLabel: 'Text',
            contentAfterLabel: 'Feedback',
            answerTextLabel: 'Answer'
        }
    },

    shouldComponentUpdate: function(nextProps, nextState) {
        return (!_.eq(this.props.model.id, nextProps.model.id) || !_.eq(this.state, nextState))
    },

    componentWillMount: function() {
        backboneReact.on(this, {
            models: {
                model: this.props.model
            }
        })
    },

    componentWillUnmount: function() {
        backboneReact.off(this)
    },

    _saveModel: _.debounce(function() {
        this.props.model.save()
    }, 1000),

    _handleTextChange: function(key, event) {
        this.props.model.set(key, event.target.value)
        this._saveModel()
    },

    _handleSelectChange: function(key, event, index, value) {
        this.props.model.set(key, value)
        this._saveModel()
    },

    _handleMultilineChange: function(key, event) {
        this.props.model.set(key, event.target.value.split('\n'))
        this._saveModel()
    },

    _handleToggleChange: function(key, event, value) {
        this.props.model.set(key, value)
        this._saveModel()
    },

    render: function() {
        var titleComponent = null
        var typeComponent = null
        var choicesComponent = null
        var preferNotToAnswerComponent = null
        var timecodeComponent = null
        var contentTextComponent = null
        var contentAfterComponent = null
        var answerTextComponent = null

        if (this.props.showTitle === true) {
            titleComponent = (
                <TextField floatingLabelText={this.props.titleLabel}
                           defaultValue={this.props.model.get('title')}
                           key={this.props.model.id + 'titleComponent'}
                           fullWidth onChange={_.partial(this._handleTextChange, 'title')}/>
            )
        }

        if (this.props.showType === true) {
            typeComponent = (
                <SelectField floatingLabelText={this.props.typeLabel} fullWidth
                             key={this.props.model.id + 'typeComponent'}
                             value={this.props.model.get('contentType')}
                             onChange={_.partial(this._handleSelectChange, 'contentType')}>
                    <MenuItem value="text" primaryText="Text" />
                    <MenuItem value="shortAnswer" primaryText="Short Answer" />
                    <MenuItem value="number" primaryText="Number" />
                    <MenuItem value="multipleChoice" primaryText="Multiple Choice" />
                    <MenuItem value="multipleNumeric" primaryText="Multiple Numeric" />
                </SelectField>
            )

            if (this.props.model.get('contentType') == 'multipleChoice'
                    || this.props.model.get('contentType') == 'multipleNumeric') {
                choicesComponent = (
                    <TextField floatingLabelText={(this.props.model.get('contentType') == 'multipleChoice'
                                    ? "Multiple Choice"
                                    : 'Multiple Numeric') + " Options (newline delimited)"}
                               multiLine fullWidth
                               key={this.props.model.id + 'choicesComponent'}
                               defaultValue={this.props.model.get('choices').join('\n')}
                               onChange={_.partial(this._handleMultilineChange, 'choices')}/>
                )
            }

            if (this.props.model.get('contentType') != 'text' &&
                    this.props.showPreferNotToAnswer === true) {
                preferNotToAnswerComponent = (
                    <div>
                        <br />
                        <Toggle defaultToggled={this.props.model.get('showPreferNotToAnswer')}
                                onToggle={_.partial(this._handleToggleChange,
                                                    'showPreferNotToAnswer')}
                                label={'Show "Prefer Not To Answer"'}
                                labelPosition="right"/>
                    </div>
                )
            }
        }

        
        if (this.props.showTimecode === true) {
            timecodeComponent = (
                <TextField floatingLabelText={this.props.timecodeLabel}
                           type="number" min="0"
                           defaultValue={this.props.model.get('timecode')} fullWidth
                           key={this.props.model.id + 'timecodeComponent'}
                           onChange={_.partial(this._handleTextChange, 'timecode')}/>
            )
        }

        if (this.props.showContentText === true) {
            contentTextComponent = (
                <TextField floatingLabelText={this.props.contentTextLabel} multiLine
                           defaultValue={this.props.model.get('contentText')} fullWidth
                           key={this.props.model.id + 'contentTextComponent'}
                           onChange={_.partial(this._handleTextChange, 'contentText')}/>
            )
        }

        if (this.props.showContentAfter === true) {
            contentAfterComponent = (
                <TextField floatingLabelText={this.props.contentAfterLabel} multiLine
                           defaultValue={this.props.model.get('contentAfter')} fullWidth
                           key={this.props.model.id + 'contentAfterComponent'}
                           onChange={_.partial(this._handleTextChange, 'contentAfter')}/>
            )
        }

        if (this.props.showAnswerText === true) {
            answerTextComponent = (
                <TextField floatingLabelText={this.props.answerTextLabel} multiLine
                           defaultValue={this.props.model.get('answer')} fullWidth
                           key={this.props.model.id + 'answerTextComponent'}
                           onChange={_.partial(this._handleTextChange, 'answer')}/>
            )
        }

        return (
            <div style={{maxHeight: '100%', height: '100%', overflowY: 'auto', overflowX: 'hidden'}}
                 key={this.props.model.id}>
                {titleComponent}
                {timecodeComponent}
                {contentTextComponent}
                {typeComponent}
                {choicesComponent}
                {preferNotToAnswerComponent}
                {contentAfterComponent}
                {answerTextComponent}
            </div>
        )
    }
})

module.exports = QuestionForm
