import os

import shutil
import zipfile
import tempfile
import mimetypes
import logging
import logging.handlers

try:
    import simplejson as json
except ImportError:
    import json

from flask import Blueprint, request, after_this_request, send_file
from werkzeug import secure_filename

try:
    from .handler import VIDEOS_PATH, DEFAULT_CONFIG_FILE, \
        PARTICIPANTS_PATH, ARCHIVES_PATH, CONFIG_DIR_PATH
    from .utils import authenticate_cas, study_handler, get_directory_structure
except (ImportError, ValueError):
    from handler import VIDEOS_PATH, DEFAULT_CONFIG_FILE, \
        PARTICIPANTS_PATH, ARCHIVES_PATH, CONFIG_DIR_PATH
    from utils import authenticate_cas, study_handler, get_directory_structure


# Logging Setup
def setup_logging():
    global log
    log = logging.getLogger(__name__)

    log_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../log/config.log')
    if not os.path.exists(os.path.dirname(log_file_path)):
        os.makedirs(os.path.dirname(log_file_path))

    hdlr = logging.handlers.RotatingFileHandler(log_file_path, maxBytes=1024000, backupCount=4)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    log.addHandler(hdlr)
    log.setLevel(logging.INFO)


setup_logging()


configbp = Blueprint('configbp', __name__, template_folder='static', static_folder='static')


@configbp.route('/api/infoletter', methods=['GET', 'PUT', 'POST'])
@configbp.route('/api/consenttext', methods=['GET', 'PUT', 'POST'])
@configbp.route('/api/feedbacktext', methods=['GET', 'PUT', 'POST'])
@authenticate_cas(restricted_methods=['POST', 'PUT', 'PATCH', 'DELETE'])
def text_request():
    if str(request.url_rule).startswith('/api/infoletter'):
        data_key = 'info_letter_text'
    elif str(request.url_rule).startswith('/api/consenttext'):
        data_key = 'consent_text'
    elif str(request.url_rule).startswith('/api/feedbacktext'):
        data_key = 'feedback_text'
    else:
        return json.dumps({'status': 'error'})

    if request.method == 'GET':
        return json.dumps({'text': getattr(study_handler, data_key)})

    request_data = request.get_json()

    text = request_data.get('text')

    setattr(study_handler, data_key, text)

    return json.dumps({'status': 'success'})


@configbp.route('/api/demographic', methods=['GET', 'POST'])
@configbp.route('/api/demographic/<string:id>', methods=['DELETE', 'PUT'])
@configbp.route('/api/probes', methods=['GET', 'POST'])
@configbp.route('/api/probes/<string:id>', methods=['DELETE', 'PUT'])
@configbp.route('/api/videos', methods=['GET', 'POST'])
@configbp.route('/api/videos/<string:id>', methods=['DELETE', 'PUT'])
@configbp.route('/api/questions', methods=['GET', 'POST'])
@configbp.route('/api/questions/<string:id>', methods=['DELETE', 'PUT'])
@authenticate_cas(restricted_methods=['POST', 'PUT', 'PATCH', 'DELETE'])
def collection_handler(id=None):
    try:
        if str(request.url_rule).startswith('/api/demographic'):
            data_key = 'demographic'
        elif str(request.url_rule).startswith('/api/probes'):
            data_key = 'probes'
        elif str(request.url_rule).startswith('/api/videos'):
            data_key = 'videos'
        elif str(request.url_rule).startswith('/api/questions'):
            data_key = 'questions'
        else:
            return json.dumps({'status': 'error'})

        if request.method == 'GET':
            return json.dumps(getattr(study_handler, data_key))
        elif request.method == 'POST':
            setattr(study_handler, data_key, request.get_json())
            return json.dumps({'status': 'success'})

        data = getattr(study_handler, data_key)

        item_found = False
        new_data = []

        for d in data:
            if d.get('id') == id:
                item_found = True
                if request.method == 'PUT':
                    new_data.append(request.get_json())
                elif request.url_rule == '/api/videos/<string:id>' and request.method == 'DELETE':
                    study_handler.probes = [p for p in study_handler.probes
                                            if p.get('video') != d.get('id')]
            else:
                new_data.append(d)

        if not item_found:
            new_data.append(request.get_json())

        setattr(study_handler, data_key, new_data)
    except Exception, e:
        log.error(e)

    return json.dumps({'status': 'success'})


@configbp.route('/api/uploadvideo', methods=['POST'])
@authenticate_cas()
def uploadvideo():
    f = request.files['videofile']

    if f and os.path.splitext(f.filename)[1] in ['.mp4', '.ogg', 'webm']:
        filename = secure_filename(f.filename)

        if not os.path.exists(os.path.dirname(VIDEOS_PATH)):
            os.makedirs(os.path.dirname(VIDEOS_PATH))

        f.save(os.path.join(VIDEOS_PATH, filename))

        videos = study_handler.videos

        for v in videos:
            if v.get('id') == filename:
                return json.dumps({'status': 'success'})

        videos.append({'id': filename, 'index': 0})
        study_handler.videos = videos

        return json.dumps({'status': 'success'})

    return json.dumps({'status': 'error'})


@configbp.route('/api/archives', methods=['GET'])
@authenticate_cas()
def archives():
    archives_obj = get_directory_structure(ARCHIVES_PATH)
    participants_obj = get_directory_structure(PARTICIPANTS_PATH)

    archives_obj.update({'current': participants_obj['participants']})

    return json.dumps({'status': 'success',
                       'files': archives_obj})


@configbp.route('/api/download_files', methods=['GET'])
@authenticate_cas()
def download_files():
    json_data = request.args
    path = json.loads(json_data.get('path'))
    if path is None or len(path) == 0:
        return json.dumps({'status': 'error'})

    starting_path = path.pop(0)

    if starting_path == 'current':
        base_dir = os.path.abspath(os.path.dirname(PARTICIPANTS_PATH))
    elif starting_path == 'archives':
        base_dir = os.path.abspath(os.path.dirname(ARCHIVES_PATH))
    else:
        return json.dumps({'status': 'error'})

    root_dir = os.path.join(base_dir, *path)

    if os.path.isfile(root_dir):
        return send_file(root_dir,
                         as_attachment=True,
                         attachment_filename=path[-1],
                         mimetype=mimetypes.guess_type(root_dir)[0])

    filename = shutil.make_archive(tempfile.mktemp(suffix='.zip'), 'zip', root_dir=root_dir)

    @after_this_request
    def cleanup(response):
        os.remove(filename)
        return response

    return send_file(filename,
                     as_attachment=True,
                     attachment_filename=path[-1] + '.zip',
                     mimetype='application/zip')


@configbp.route('/api/delete_files', methods=['GET'])
@authenticate_cas()
def delete_files():
    json_data = request.args
    path = json.loads(json_data.get('path'))
    if path is None or len(path) == 0:
        return json.dumps({'status': 'error'})

    starting_path = path.pop(0)

    if starting_path == 'current':
        base_dir = os.path.abspath(os.path.dirname(PARTICIPANTS_PATH))
    elif starting_path == 'archives':
        base_dir = os.path.abspath(os.path.dirname(ARCHIVES_PATH))
    else:
        return json.dumps({'status': 'error'})

    file_path = os.path.join(base_dir, *path)

    if os.path.isfile(file_path):
        os.remove(file_path)
    elif os.path.isdir(file_path):
        shutil.rmtree(file_path)
    else:
        return json.dumps({'status': 'error'})

    if file_path == base_dir and starting_path == 'current':
        os.makedirs(PARTICIPANTS_PATH)

    return json.dumps({'status': 'success'})


@configbp.route('/api/archive_files', methods=['GET'])
@authenticate_cas()
def archive_files():
    participants_path = os.path.abspath(os.path.dirname(PARTICIPANTS_PATH))
    archives_path = os.path.abspath(os.path.dirname(ARCHIVES_PATH))

    if not os.path.exists(archives_path):
        os.makedirs(archives_path)

    tempname = tempfile.mkdtemp(prefix='Archive_', dir=archives_path)
    os.removedirs(tempname)
    shutil.copytree(participants_path, os.path.join(archives_path, tempname))
    return json.dumps({'status': 'success', 'archive': os.path.basename(tempname)})


@configbp.route('/api/rename_file', methods=['GET'])
@authenticate_cas()
def rename_file():
    json_data = request.args
    old_name = json_data.get('oldname')
    new_name = json_data.get('newname')
    if old_name is None or new_name is None:
        return json.dumps({'status': 'error'})

    archives_path = os.path.abspath(os.path.dirname(ARCHIVES_PATH))

    if not os.path.exists(archives_path):
        os.makedirs(archives_path)

    old_path = os.path.join(archives_path, old_name)
    new_path = os.path.join(archives_path, new_name)

    if not os.path.exists(old_path):
        return json.dumps({'status': 'error'})

    shutil.move(old_path, new_path)

    return json.dumps({'status': 'success'})


@configbp.route('/api/importconfig', methods=['POST'])
@authenticate_cas()
def import_config():
    f = request.files['configfile']

    if f and os.path.splitext(f.filename)[1] == '.xml':
        if not os.path.exists(os.path.dirname(DEFAULT_CONFIG_FILE)):
            os.makedirs(os.path.dirname(DEFAULT_CONFIG_FILE))

        f.save(DEFAULT_CONFIG_FILE)

        return json.dumps({'status': 'success'})
    elif f and os.path.splitext(f.filename)[1] == '.zip':
        shutil.rmtree(CONFIG_DIR_PATH)

        if not os.path.exists(CONFIG_DIR_PATH):
            os.makedirs(CONFIG_DIR_PATH)

        zfile = zipfile.ZipFile(f)
        zfile.extractall(CONFIG_DIR_PATH)

    return json.dumps({'status': 'error'})


@configbp.route('/api/exportconfig')
@authenticate_cas()
def export_config():
    base_dir = os.path.abspath(os.path.dirname(CONFIG_DIR_PATH))
    filename = shutil.make_archive(tempfile.mktemp(suffix='.zip'), 'zip', root_dir=base_dir)

    @after_this_request
    def cleanup(response):
        os.remove(filename)
        return response

    return send_file(filename,
                     as_attachment=True,
                     attachment_filename='configuration.zip',
                     mimetype='application/zip')


@configbp.route('/api/downloaddata')
@authenticate_cas()
def download_data():
    base_dir = os.path.abspath(os.path.dirname(PARTICIPANTS_PATH))
    filename = shutil.make_archive(tempfile.mktemp(suffix='.zip'), 'zip', root_dir=base_dir)

    @after_this_request
    def cleanup(response):
        os.remove(filename)
        return response

    return send_file(filename,
                     as_attachment=True,
                     attachment_filename='participantData.zip',
                     mimetype='application/zip')


