try:
    import simplejson as json
except ImportError:
    import json

import csv
import logging
import logging.handlers
import os
import hashlib
import random
from datetime import date

from flask import Flask, request, render_template, redirect, session, url_for
from flask.ext.cas import CAS
from werkzeug import secure_filename
from werkzeug.serving import run_simple

try:
    from .handler import VIDEOS_PATH, DEFAULT_CONFIG_FILE, PARTICIPANTS_PATH
    from .config import configbp
    from .utils import authenticate_cas, authenticate_participant, utilsbp, AppWrapper, send_file_206, study_handler
except (ImportError, ValueError):
    from handler import VIDEOS_PATH, DEFAULT_CONFIG_FILE, PARTICIPANTS_PATH
    from config import configbp
    from utils import authenticate_cas, authenticate_participant, utilsbp, AppWrapper, send_file_206, study_handler


SPECIAL_TEST_USER = '4ff8c714b4214732bfd4bac0eba65b6a'
SECRET_KEY = 'D1221F6A6E9944448E277492D06B7598'

VALID_ACCESS_PARTS = ['infoletter', 'consent', 'demographic', 'video', 'test', 'feedback']


# Flask App Setup
def setup_app():
    global app
    app = Flask(__name__, template_folder='static', static_folder='static', static_path='')
    CAS(app)
    app.config['CAS_SERVER'] = 'https://cas.uwaterloo.ca/cas'
    app.config['CAS_AFTER_LOGIN'] = ''
    app.secret_key = SECRET_KEY
    app.register_blueprint(configbp)
    app.register_blueprint(utilsbp)


# Logging Setup
def setup_logging():
    global log
    log = logging.getLogger(__name__)

    log_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../log/main.log')
    if not os.path.exists(os.path.dirname(log_file_path)):
        os.makedirs(os.path.dirname(log_file_path))

    hdlr = logging.handlers.RotatingFileHandler(log_file_path, maxBytes=1024000, backupCount=4)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    log.addHandler(hdlr)
    log.setLevel(logging.INFO)


setup_app()
setup_logging()


@app.route('/participate')
def root():
    userid = request.args.get('workerId')

    if not userid:
        return "The link you were given was incorrect. Please contact the system administrator " \
               "if you believe this is not the case"

    participantdir = os.path.join(PARTICIPANTS_PATH, date.today().isoformat(), secure_filename(userid))

    session['userid'] = userid
    session['participantdir'] = participantdir
    session['condition'] = ''

    return redirect(url_for('index'))


@app.route('/index', methods=['GET'])
@authenticate_participant()
def index():
    for dir, _, _ in os.walk(PARTICIPANTS_PATH):
        if session['userid'] == os.path.basename(dir):
            return redirect('repeat_participation/' + session['userid'])

    num_conditions = len(study_handler.videos)

    if num_conditions == 0:
        return redirect('invalid_configuration')

    condition_num = random.randint(0, num_conditions - 1)
    condition = study_handler.videos[condition_num]

    session['condition'] = condition.get('id')
    session['pauseblur'] = condition.get('pauseblur')

    return render_template('index.html')


@app.route('/api/condition', methods=['GET'])
@authenticate_participant()
def get_condition():
    return json.dumps({'status': 'success', 'video': session['condition'], 'pauseblur': session['pauseblur']})


@app.route('/api/submit_consent', methods=['POST'])
@authenticate_participant()
def submit_consent():
    userid = session['userid']
    participantdir = session['participantdir']

    if not os.path.exists(participantdir):
        os.makedirs(participantdir)

    user_file = os.path.join(session['participantdir'], 'condition.txt')
    with open(user_file, 'w') as condition_file:
        condition_file.write(session['condition'])

    return json.dumps({'status': 'success'})


@app.route('/api/demographic_answers', methods=['POST'])
@app.route('/api/probe_answers', methods=['POST'])
@app.route('/api/question_answers', methods=['POST'])
@authenticate_participant()
def save_responses():
    if str(request.url_rule) == '/api/demographic_answers':
        filename = 'demographic.csv'
    elif str(request.url_rule) == '/api/probe_answers':
        filename = 'probes.csv'
    elif str(request.url_rule) == '/api/question_answers':
        filename = 'test.csv'

    fieldnames = ['subject_id', 'question_id', 'title', 'answer', 'content_text',
                  'prefer_not_to_answer', 'start_timestamp', 'change_timestamp',
                  'submit_timestamp', 'end_timestamp']

    if str(request.url_rule) == '/api/question_answers':
        fieldnames.append('timing')

    user_file = os.path.join(session['participantdir'], filename)

    with open(user_file, 'w') as csvfile:
        writer = csv.DictWriter(csvfile,
                                fieldnames=fieldnames,
                                extrasaction='ignore',
                                quoting=csv.QUOTE_ALL)
        writer.writeheader()

        for answer in request.get_json():
            answer['question_id'] = answer.get('id')
            answer['subject_id'] = session['userid']
            writer.writerow(dict((k, v.encode('utf-8') if isinstance(v, unicode) else v)
                                 for k, v in answer.iteritems()))

    return json.dumps({'status': 'success'})


@app.route('/api/distractions', methods=['POST'])
@authenticate_participant()
def save_distractions():
    filename = 'distractions.csv'

    fieldnames = ['subject_id', 'type', 'timestamp']
    user_file = os.path.join(session['participantdir'], filename)

    with open(user_file, 'w') as csvfile:
        writer = csv.DictWriter(csvfile,
                                fieldnames=fieldnames,
                                extrasaction='ignore',
                                quoting=csv.QUOTE_ALL)
        writer.writeheader()

        for blur in request.get_json():
            blur['subject_id'] = session['userid']
            writer.writerow(dict((k, v.encode('utf-8') if isinstance(v, unicode) else v)
                                 for k, v in blur.iteritems()))

    return json.dumps({'status': 'success'})


@app.route('/api/video_timestamps', methods=['POST'])
@authenticate_participant()
def save_video_timestamps():
    filename = 'videotimestamps.txt'

    user_file = os.path.join(session['participantdir'], filename)
    request_json = request.get_json()

    with open(user_file, 'w') as txtfile:
        txtfile.write('%s\n%s' % (request_json.get('start_timestamp', ''), request_json.get('end_timestamp', '')))

    return json.dumps({'status': 'success'})



@app.route('/api/get_participant_key')
def get_participant_key():
    participant_key = hashlib.sha256(session['userid'] + SECRET_KEY).hexdigest()

    user_file = os.path.join(session['participantdir'], 'key.txt')

    with open(user_file, 'w') as keyfile:
        keyfile.write(participant_key)

    return json.dumps({'status': 'success', 'key': participant_key})


@app.route('/config', methods=['GET'])
@authenticate_cas()
def config():
    return render_template('config.html')


@app.route('/videos/<path:path>')
def send_video(path):
    '''
    Taken from http://blog.asgaard.co.uk/2012/08/03/http-206-partial-content-for-flask-python
    '''
    path = os.path.join(VIDEOS_PATH, secure_filename(path))
    return send_file_206(path)


def main():
    global app
    app = AppWrapper(app)
    run_simple('0.0.0.0', 5001, app, use_debugger=True, threaded=True)


if __name__ == '__main__':
    main()
