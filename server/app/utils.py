import mimetypes
import os
from functools import wraps

import re
from flask import Blueprint, session, redirect, request, Response, send_from_directory, send_file, stream_with_context
from flask.ext.cas import login
from werkzeug.datastructures import Headers
from werkzeug.wsgi import FileWrapper

try:
    from .handler import StudyHandler, PARTICIPANTS_PATH
except (ImportError, ValueError):
    from handler import StudyHandler, PARTICIPANTS_PATH

CAS_WHITELIST = ['e2lavign', 'efrisko', 'k42wilso', 'l24marti', 'ma4marti']


# Study Handler Setup
study_handler = StudyHandler()


utilsbp = Blueprint('utilsbp', __name__, template_folder='static', static_folder='static')


@utilsbp.route('/no_access')
def no_access():
    return 'You do not have permission to view this page. Please contact your system administrator for access.'


@utilsbp.route('/repeat_participation/<userid>')
def repeat_participation(userid):
    return 'Our records have shown that you have already participated in this study using this User ID (%s). ' \
           'You may only participate once.' % userid


@utilsbp.route('/invalid_configuration')
def invalid_configuration():
    return 'This study had not yet been set up for participation yet. Please contact the system administrator if ' \
           'you believe this to be inaccurate'


@utilsbp.route('/invalid_id')
def invalid_id():
    return 'The link you have accessed is invalid. ' \
           'Please contact the study administrator to resolve your problem'


def authenticate_participant():
    def outerwrap(func):
        @wraps(func)
        def wrap(*args, **kwargs):
            userid = session.get('userid')
            participantdir = session.get('participantdir')
            condition = session.get('condition')

            # if userid is None or participantdir is None or condition is None:
            #     return redirect('invalid_id')

            return func(*args, **kwargs)

        return wrap

    return outerwrap


def authenticate_cas(restricted_methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE']):
    def outerwrap(func):
        @wraps(func)
        def wrap(*args, **kwargs):
            if request.method in restricted_methods:
                if 'CAS_USERNAME' not in session:
                    session['CAS_AFTER_LOGIN_SESSION_URL'] = request.url
                    return login()

                if session.get('CAS_USERNAME') not in CAS_WHITELIST:
                    return redirect('no_access')

            return func(*args, **kwargs)

        return wrap

    return outerwrap


def get_directory_structure(rootdir):
    """
    Creates a nested dictionary that represents the folder structure of rootdir
    """
    dir = {}
    rootdir = rootdir.rstrip(os.sep)
    start = rootdir.rfind(os.sep) + 1
    for path, dirs, files in os.walk(rootdir):
        folders = path[start:].split(os.sep)
        subdir = dict.fromkeys(files)
        parent = reduce(dict.get, folders[:-1], dir)
        parent[folders[-1]] = subdir
    return dir


# class PartialFileWrapper(object):
#     def __init__(self, f, offset=0, length=None, buffer_size=8192):
#         self.file = f
#         self.file.seek(offset, os.SEEK_CUR)
#         self.length = length
#         self.buffer_size = buffer_size
#
#     def close(self):
#         if hasattr(self.file, 'close'):
#             self.file.close()
#
#     def __iter__(self):
#         return self
#
#     def next(self):
#         if self.length is not None:
#             if self.length <= 0:
#                 raise StopIteration()
#             data = self.file.read(min(self.length, self.buffer_size))
#             if not data:
#                 raise StopIteration()
#             if len(data) == 0:
#                 raise StopIteration()
#             self.length -= len(data)
#             return data
#         else:
#             data = self.file.read(self.buffer_size)
#             if data:
#                 return data
#             raise StopIteration()
#
#
# def send_file_partial(path, attachment_filename=None, headers=Headers()):
#     headers.add('Content-Disposition', 'attachment', filename=attachment_filename)
#
#     f = open(path, 'rb')
#     size = os.path.getsize(path)
#     headers.add('Accept-Ranges', 'bytes')
#     range_header = request.headers.get('Range', None)
#     if not range_header:
#         headers.add('Content-Length', size)
#         return Response(FileWrapper(f), mimetype=mimetype, headers=headers, direct_passthrough=True)
#
#     byte1, byte2 = 0, None
#     m = re.search('(\d+)-(\d*)', range_header)
#     g = m.groups()
#     if g[0]:
#         byte1 = int(g[0])
#     if g[1]:
#         byte2 = int(g[1])
#
#     length = size - byte1
#     if byte2 is not None:
#         length = byte2 - byte1
#     headers.add('Content-Range', 'bytes {0}-{1}/{2}'.format(byte1, byte1 + length - 1, size))
#     headers.add('Content-Length', length)
#
#     return Response(stream_with_context(PartialFileWrapper(f, offset=byte1, length=length)), 206,
#                     mimetype='video/' + os.path.splitext(f.name)[1][1:], headers=headers,
#                     direct_passthrough=True)


def send_file_206(path):
    range_header = request.headers.get('Range', None)
    if not range_header:
        return send_file(path)

    size = os.path.getsize(path)
    byte1, byte2 = 0, None

    m = re.search('(\d+)-(\d*)', range_header)
    g = m.groups()

    if g[0]:
        byte1 = int(g[0])
    if g[1]:
        byte2 = int(g[1])

    length = size - byte1
    if byte2 is not None:
        length = byte2 - byte1 + 1

    data = None
    with open(path, 'rb') as f:
        f.seek(byte1)
        data = f.read(length)

    rv = Response(data, 206, mimetype=mimetypes.guess_type(path)[0], direct_passthrough=True)
    rv.headers.add('Content-Range', 'bytes {0}-{1}/{2}'.format(byte1, byte1 + length - 1, size))
    return rv


class AppWrapper(object):
    allowed_methods = frozenset([
        'GET',
        'HEAD',
        'POST',
        'DELETE',
        'PUT',
        'PATCH',
        'OPTIONS'
    ])
    bodyless_methods = frozenset(['GET', 'HEAD', 'OPTIONS', 'DELETE'])

    def __init__(self, app, script_name=None):
        self.app = app
        self.script_name = script_name

    def __call__(self, environ, start_response):
        if self.script_name is not None:
            environ['SCRIPT_NAME'] = self.script_name

        method = environ.get('HTTP_X_HTTP_METHOD_OVERRIDE', '').upper()
        if method in self.allowed_methods:
            method = method.encode('ascii', 'replace')
            environ['REQUEST_METHOD'] = method
        if method in self.bodyless_methods:
            environ['CONTENT_LENGTH'] = '0'

        return self.app(environ, start_response)
