try:
    import simplejson as json
except ImportError:
    import json

import os
import logging
import logging.handlers

import bleach


# Logging Setup
def setup_logging():
    global log
    log = logging.getLogger(__name__)

    log_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../log/handler.log')
    if not os.path.exists(os.path.dirname(log_file_path)):
        os.makedirs(os.path.dirname(log_file_path))

    hdlr = logging.handlers.RotatingFileHandler(log_file_path, maxBytes=1024000, backupCount=4)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    log.addHandler(hdlr)
    log.setLevel(logging.INFO)


setup_logging()

CUSTOM_ALLOWED_TAGS = bleach.ALLOWED_TAGS + ['div', 'br', 'span', 'u', 's',
                                             'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
                                             'p', 'img', 'a', 'ul', 'ol', 'li', 'sub', 'sup']
CUSTOM_ALLOWED_ATTRIBUTES = bleach.ALLOWED_ATTRIBUTES
CUSTOM_ALLOWED_ATTRIBUTES.update({
    '*': ['style']
})

CUSTOM_ALLOWED_STYLES = bleach.ALLOWED_STYLES + [
    'text-align',
    'background-color',
    'color',
    'direction',
    'font-family',
    'font-size'
]

CONFIG_DIR_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../data/config/')
DEFAULT_CONFIG_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../data/config/config.xml')
VIDEOS_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../data/config/videos/')
PARTICIPANTS_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../data/participants/')
ARCHIVES_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../data/archives/')


INFO_LETTER_FILENAME = 'infoletter.txt'
CONSENT_FILENAME = 'consent.txt'
DEMOGRAPHIC_FILENAME = 'demographic.json'
FEEDBACK_FILENAME = 'feedback.txt'
PROBES_FILENAME = 'probes.json'
VIDEOS_FILENAME = 'videos.json'
QUESTIONS_FILENAME = 'questions.json'


class StudyHandler(object):
    _config_dir = CONFIG_DIR_PATH
    _info_letter_path = None
    _consent_path = None
    _demographic_path = None
    _feedback_path = None
    _probes_path = None
    _questions_path = None

    def __init__(self, config_dir=CONFIG_DIR_PATH):
        if not os.path.exists(config_dir):
            os.makedirs(config_dir)

        for filename in [INFO_LETTER_FILENAME, CONSENT_FILENAME, DEMOGRAPHIC_FILENAME,
                         FEEDBACK_FILENAME, PROBES_FILENAME, VIDEOS_FILENAME, QUESTIONS_FILENAME]:
            if not os.path.isfile(os.path.join(self._config_dir, filename)):
                with open(os.path.join(self._config_dir, filename), 'w+') as f:
                    pass

        self._config_dir = config_dir


    # ******************************Info Letter******************************
    @property
    def info_letter_text(self):
        with open(os.path.join(self._config_dir, INFO_LETTER_FILENAME), 'r') as f:
            text = f.read().decode('utf8')
            return bleach.clean(text, tags=CUSTOM_ALLOWED_TAGS,
                                attributes=CUSTOM_ALLOWED_ATTRIBUTES, styles=CUSTOM_ALLOWED_STYLES)

    @info_letter_text.setter
    def info_letter_text(self, value):
        with open(os.path.join(self._config_dir, INFO_LETTER_FILENAME), 'w+') as f:
            f.write(value.encode('utf8'))

    # ******************************Consent******************************
    @property
    def consent_text(self):
        with open(os.path.join(self._config_dir, CONSENT_FILENAME), 'r') as f:
            text = f.read().decode('utf8')
            return bleach.clean(text, tags=CUSTOM_ALLOWED_TAGS,
                                attributes=CUSTOM_ALLOWED_ATTRIBUTES, styles=CUSTOM_ALLOWED_STYLES)

    @consent_text.setter
    def consent_text(self, value):
        with open(os.path.join(self._config_dir, CONSENT_FILENAME), 'w+') as f:
            f.write(value.encode('utf8'))

    # ******************************Probes******************************
    _default_demographic_item = {'id': None, 'index': 0, 'title': '', 'content_text': '', 'content_type': '',
                                 'choices': [], 'show_prefer_not_to_answer': False}

    @property
    def demographic(self):
        with open(os.path.join(self._config_dir, DEMOGRAPHIC_FILENAME), 'r') as f:
            try:
                demographic_json = json.loads(f.read())
            except ValueError:
                demographic_json = []

            res = []
            for d in demographic_json:
                new_d = dict(self._default_demographic_item)
                new_d.update(d)
                res.append(new_d)

            return res

    @demographic.setter
    def demographic(self, value):
        with open(os.path.join(self._config_dir, DEMOGRAPHIC_FILENAME), 'w+') as f:
            f.write(json.dumps(value, sort_keys=True, indent=4, separators=(',', ': ')))

    # ******************************Feedback******************************
    @property
    def feedback_text(self):
        with open(os.path.join(self._config_dir, FEEDBACK_FILENAME), 'r') as f:
            text = f.read().decode('utf8')
            return bleach.clean(text, tags=CUSTOM_ALLOWED_TAGS,
                                attributes=CUSTOM_ALLOWED_ATTRIBUTES, styles=CUSTOM_ALLOWED_STYLES)

    @feedback_text.setter
    def feedback_text(self, value):
        with open(os.path.join(self._config_dir, FEEDBACK_FILENAME), 'w+') as f:
            f.write(value.encode('utf8'))

    # ******************************Probes******************************

    _default_probe_item = {'id': None,
                           'index': 0,
                           'title': '',
                           'content_text': '',
                           'content_type': '',
                           'content_after': '',
                           'video': '',
                           'timecode': 0,
                           'choices': [],
                           'show_prefer_not_to_answer': False}

    @property
    def probes(self):
        with open(os.path.join(self._config_dir, PROBES_FILENAME), 'r') as f:
            try:
                probe_json = json.loads(f.read())
            except ValueError:
                probe_json = []

            res = []
            for p in probe_json:
                new_p = dict(self._default_probe_item)
                new_p.update(p)
                res.append(new_p)

            return res


    @probes.setter
    def probes(self, value):
        with open(os.path.join(self._config_dir, PROBES_FILENAME), 'w+') as f:
            f.write(json.dumps(value, sort_keys=True, indent=4, separators=(',', ': ')))

    # ******************************Videos******************************

    _default_video_item = {'id': None,'index': 0, 'pauseblur': False}

    @property
    def videos(self):
        with open(os.path.join(self._config_dir, VIDEOS_FILENAME), 'r') as f:
            try:
                video_json = json.loads(f.read())
            except ValueError:
                video_json = []

            res = []
            for v in video_json:
                new_v = dict(self._default_video_item)
                new_v.update(v)
                res.append(new_v)

            return res


    @videos.setter
    def videos(self, value):
        with open(os.path.join(self._config_dir, VIDEOS_FILENAME), 'w+') as f:
            f.write(json.dumps(value, sort_keys=True, indent=4, separators=(',', ': ')))

        videos_found = [v.get('id') for v in value]

        for _, _, files in os.walk(VIDEOS_PATH):
            for f in files:
                if f not in videos_found:
                    os.remove(os.path.join(VIDEOS_PATH, f))

    # ******************************Questions******************************
    _default_question_item = {'id': None,
                              'index': 0,
                              'title': '',
                              'content_text': '',
                              'content_type': '',
                              'content_after': '',
                              'choices': [],
                              'timing': 'after',
                              'answer': None,
                              'show_prefer_not_to_answer': False}

    @property
    def questions(self):
        with open(os.path.join(self._config_dir, QUESTIONS_FILENAME), 'r') as f:
            try:
                questions_json = json.loads(f.read())
            except ValueError:
                questions_json = []

            res = []
            for q in questions_json:
                new_q = dict(self._default_question_item)
                new_q.update(q)
                res.append(new_q)

            return res


    @questions.setter
    def questions(self, value):
        with open(os.path.join(self._config_dir, QUESTIONS_FILENAME), 'w+') as f:
            f.write(json.dumps(value, sort_keys=True, indent=4, separators=(',', ': ')))
